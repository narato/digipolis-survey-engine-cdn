# Survey Engine CDN

Je kan zelf controls en wrappers toevoegen in een CDN die op hun beurt kunnen opgehaald worden in de F&S renderer.

De structuur van de CDN is als volgt:

    CDN  
    |- controls  
    |- wrappers  

## Controls
De filestructuur van een custom control kan er als volgt uitzien:

    text  
    |- control.json  
    |- text.html  
    |- text.js  

De naam van de map bevat de naam van de control. Deze naam wordt gebruikt als referentie om de control te kunnen ophalen.

### control.json (verplicht)
Deze file moet volgende structuur hebben:

    {  
        "control": {  
            "name": "Text Control",  
            "key": "text",  
            "wrapper": "default",  
            "files": {  
                "code": [  
                    "text.js"  
                ],  
                "styles": "",  
                "template": "text.html"  
            }  
        }  
    }  

- name: Volledige naam van de control
- key: Bepaalt de referentie in een form schema naar de control
- wrapper: referentie naar de wrapper rond de control, wrappers zitten in een aparte map in de CDN (zie verder)
- viewWrapper (optioneel): naam van de wrapper in read-only mode
- files: De root is telkens de map van de control
    - code: array van javascript files
    - styles: naam van css bestand in dezelfde map
    - template: naam van html bestand
    - viewTemplate (optioneel): naam van de template voor de control in read-only mode

### text.js
Bepaalt het gedrag van de control. De code moet toegevoegd worden aan een bestaande module uit de F&S renderer Angular module ‘formRenderer.controls’.

    var module = angular.module('formRenderer.controls');
    // add any specific code for the control to this module

### text.html
Voorbeeld:

    <div class="fr-field-state-write-wrapper" ng-show="field.state.editMode">  
        <input  
            ng-model="ngModel"  
            ng-class="{  
                'invalid': field.validation.error.length > 0,  
                'valid': field.validation.error.length === 0  
            }"  
            fr-validation  
            data-validation="field.validation"  
            id="{{ field.attrs.id }}"  
            name="{{ field.attrs.name }}"  
            class="field full"  
            type="text"  
            placeholder="{{ field.attrs.placeholder }}" />  
    </div>  
    <div class="fr-field-state-read-wrapper">  
        <p class="fr-p" ng-if="!field.state.editMode">{{ngModelInit}} <span ng-if="field.to.unit"   ng-bind-html="field.to.unit | unsafe"></span></p>  
    </div>  

#Wrappers
De filestructuur van een wrapper kan er als volgt uitzien:

    default  
    |- wrapper.html  

Voorbeeld van een wrapper:


    <div class="fr-field-{{ field.baseType }} field-container">  
        <span>  
          <label style="color: black; padding-left: 0;" class="label" for="" data-ng-class="{'required': field.validation.required && field.state.editMode}">{{field.to.label}}</label>
      </span>  
      <fr-transclude></fr-transclude>  
    </div>  
