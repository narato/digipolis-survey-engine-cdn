(function (_, ng) {
    'use strict';
    ng
    .module('formRenderer.controls')
    .service('frAstadFieldTypeTextarea', [

        /**
         * frAstadFieldTypeTextareaService
         * @return {Object} - field type configuration object
         */
        function frAstadFieldTypeTextareaService() {
            return {
                name: 'textarea',
                templateUrl: 'views/types/textarea.html',
                wrapper: ['astadFieldWrapper'],
                viewTemplateUrl: 'views/types/view-default.html',
                viewWrapper: ['astadViewFieldWrapper'],
                controller: function ($scope) {

                    /**
                     * @function
                     * @name initialize -------------------------------
                     * @description - initialize textarea
                     */
                    function initialize() {
                        if (!$scope.field.state.editMode && _.isString($scope.ngModelInit)) {
                            $scope.ngModelInit = replaceNewlinesWithBrTags($scope.ngModelInit);
                        }
                    }

                    /**
                     * @function
                     * @name replaceNewlinesWithBrTags ----------------
                     * @description - replace newlines to html br tags
                     */
                    function replaceNewlinesWithBrTags(string) {
                        string = string.replace(new RegExp('\r?\n', 'g'), '<br />');
                        return string;
                    }

                    /**
                     * @function
                     * @name replaceBrTagsWithNewlines ----------------
                     * @description - replace newlines to html br tags
                     */
                    function replaceBrTagsWithNewlines(string) {
                        string = string.replace(/<br\s*[\/]?>/gi, '\n');
                        return string;
                    }

                    $scope.$watch('field.state.editMode', function watchEditMode(newvalue, oldValue) {
                        if (newvalue !== oldValue) {
                            if (newvalue) {
                                $scope.ngModel = replaceBrTagsWithNewlines($scope.ngModelInit);
                            } else {
                                $scope.ngModel = replaceNewlinesWithBrTags($scope.ngModelInit);
                            }
                        }
                    });

                    initialize();
                }
            };
        }
    ]);
})(window._, window.angular);
