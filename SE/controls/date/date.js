(function (ng) {
    'use strict';
    ng.module('formRenderer.controls').service('frAstadFieldTypeDate', [
        '$document',
        function frAstadFieldTypeDateService($document) {
            return {
                name: 'date',
                templateUrl: 'views/types/date.html',
                wrapper: ['astadFieldWrapper'],
                viewTemplateUrl: 'views/types/view-default.html',
                viewWrapper: [],
                link: function (scope, element, attrs) {
                    /**
                     * @function
                     * @name initialize -------------------------------------
                     */
                    function initialize() {
                        getCurrentDate();
                    }

                    /**
                     * @function
                     * @name getCurrentDate ---------------------------------
                     * @description get todays date
                     */
                    function getCurrentDate() {
                        var date = new Date();
                        scope.currentDate = date.getDate();
                    }

                    scope.state = {
                        picker: false
                    };

                    /**
                     * @function
                     * @name  toggleDatePicker ------------------------------
                     * @description toggle datepicker state
                     */
                    scope.toggleDatepicker = function toggleDatepicker() {
                        if (scope.field.state.editMode) {
                            scope.state.picker =  !scope.state.picker;
                        }
                    };

                    /**
                     * @function
                     * @name  onDatePicked -----------------------------------
                     * @description this function will envoke when the user has
                     * picked a date
                     */
                    scope.onDatePicked = function onDatePicked(date) {
                        scope.state.picker = false;
                    };

                    initialize();

                }
            };
        }
    ]);
})(window.angular);
